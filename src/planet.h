/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "dada.h"
#include "entity.h"
#include <string>
#include <vector>

#ifndef GEOPLANET
#define GEOPLANET

namespace geogame {

  class planet : public entity {
  public:
    planet(d_vector pos, d_vector dir, float mass, d_vector col, float size);
  
    virtual void update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist);
    virtual void render();
    void set_velocity(d_vector v) { m_velocity=v; };
    virtual float get_mass() { return m_mass; }
    virtual bool check_collide(const d_vector &pos, float radius, bool damage);
    d_vector m_col;
  
  private:
    float m_mass;
    float m_size;
  };

}

#endif

