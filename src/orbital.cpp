/*  Copyright (C) 2019 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "orbital.h"
#include "entity.h"

using namespace geogame;

//const double GRAVITY = 0.000000000066742;
const double GRAVITY = 0.000066742;
  
float orbital::orbital_speed(float orbit_radius, float central_mass) {  
  return sqrt((GRAVITY*central_mass)/orbit_radius);
}

d_vector orbital::calc_gravity(const list<entity*> &world, entity *e, d_vector pos, float mass, bool &collapsed) {
  d_vector ret(0,0,0);
  
  for (list<entity*>::const_iterator i=world.begin(); i!=world.end(); i++) {
    if (e!=(*i) && (*i)->get_mass()>MIN_MASS) {
      d_vector bet=(*i)->get_position()-pos;
      float dist = bet.mag();
      if (dist<0.01) {
	//	collapsed=true;
      }
      if (dist<1) {
	dist=1;
      }
      float g_force = (GRAVITY * (*i)->get_mass() * mass)/pow(dist,2);
      bet.normalise();
      if (mass>MIN_MASS) {
	ret+=bet*(g_force/mass);
      } else {
	ret+=bet*(g_force/MIN_MASS);
      }
    }
  }
  
  return ret;
}

d_vector orbital::orbital_velocity(const d_vector &pos, entity *star) {
  d_vector pos2star=pos-star->get_position();
  float speed = orbital_speed(pos2star.mag(),star->get_mass());
  pos2star.normalise();
  d_vector ret(-pos2star.y,pos2star.x,0);
  ret*=speed;
  return ret;
}
