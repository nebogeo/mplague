/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "dada.h"

#ifndef ORBITAL
#define ORBITAL

namespace geogame {

class entity;

 const double MIN_MASS = 0.0001;
 
 class orbital {
 public:
   static d_vector calc_gravity(const list<entity*> &world, entity *e, d_vector pos, float mass, bool &collapsed);
   static float orbital_speed(float orbit_radius, float central_mass); 
   static d_vector orbital_velocity(const d_vector &pos, entity *star);
 };
 
}

#endif
