/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "entity.h"
#include "orbital.h"

using namespace geogame;

unsigned int entity::m_next_id=1;

void entity::update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist) {
  m_new_entities.clear();
}

void entity::post_update() {
  m_last_position=m_position;
}

const d_vector entity::gravity(const list<entity*> &world) {
  bool collapsed=false;
  d_vector ret = orbital::calc_gravity(world, this, get_position(), get_mass(), collapsed);
  if (collapsed) {
    m_remove_me=true;
  }
  return ret;
}

