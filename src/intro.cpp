/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "glinc.h"
#include <iostream>
#include <unistd.h>
#include "intro.h"
#include "utils.h"
#include "feeble.h"
#include "bleep.h"

using namespace geogame;

bool intro::m_init_audio=false;
int intro::m_menu_sound;
int intro::m_select_sound;
int intro::m_BGsound;
int intro::m_BG2sound;

intro::intro(int width, int height, geo_game *game):
  m_width(width),
  m_height(height),
  m_current_screen(NULL),
  m_game(game),
  m_finished(false),
  m_first_time(true),
  m_deadline(1/15.0f),
  m_time(0),
  m_delta(0)
{
  if (!m_init_audio) {
    init_audio();
  }

  bleep::get()->play(m_BGsound, 220, 1, 1);
  bleep::get()->play(m_BGsound, 330, 1, 2);
  bleep::get()->play(m_BG2sound, 220, 1, 3);

  for (int n=0; n<10; n++) {
    d_matrix m;
    m.translate(rand_range(-2,2),rand_range(-2,2),-10);
    //m.translate(0,0,-10);
    m.scale(0.5,0.5,0.5);
    m_geos.push_back(new geo(0,m));
  }

  list<entity_command> cmdlist;
  for (list<geo*>::iterator i=m_geos.begin(); i!=m_geos.end(); i++) {
    (*i)->randomise();
    for (int n=0; n<100; n++) (*i)->process();
    list<entity*> world;
    (*i)->update(world,d_vector(0,0,0),cmdlist);
  }
}

intro::~intro()
{
}

void intro::init()
{
  bleep::get()->play(m_BGsound, 220, 1, 1);
  bleep::get()->play(m_BGsound, 330, 1, 2);
  bleep::get()->play(m_BG2sound, 220, 1, 3);
}

void intro::build(const string &screen_name, const string &desc)
{
  screen* newscreen = new screen(m_game);
#ifdef WIN32
  istringstream stream(desc.c_str());
#else
  istringstream stream(desc);
#endif
  newscreen->build(stream);
  m_screens[screen_name]=newscreen;
  m_current_screen=newscreen;
}

void intro::handle(unsigned char key, int button, int state, int x, int y)
{
  m_current_screen->handle(key,button,state,x,y);
}

void intro::update()
{
  m_current_screen->update();
  string token=m_current_screen->get_token();
  if (token!="") {
    // see if it's asked to switch to a new screen
    map<string,screen*>::iterator i=m_screens.find(token);
    if (i!=m_screens.end()) {
      m_current_screen=i->second;
    }

    list<entity_command> cmdlist;
    for (list<geo*>::iterator i=m_geos.begin(); i!=m_geos.end(); i++) {
      (*i)->randomise();
      for (int n=0; n<100; n++) (*i)->process();
      list<entity*> world;
      (*i)->update(world,d_vector(0,0,0),cmdlist);
    }

    if (token=="exit") {
      exit(0);
    }

    if (token=="finished") {
      m_finished=true;
      bleep::get()->stop_loop(1);
      bleep::get()->stop_loop(2);
      bleep::get()->stop_loop(3);
    }
  }
}

void intro::reshape(int width, int height)
{
  m_width=width;
  m_height=height;
  m_first_time=true;
}

void intro::render()
{
  if (m_first_time) {
    glViewport(0,0,m_width,m_height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity();
    //gl_ortho(-1,1,-0.7,0.7,0.1,100);
    glOrtho(-13,13,-7,7,0.1,100);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    m_first_time=false;
  }

  glClear(GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glPushMatrix();
  glScalef(5,5,5);
  // render the bg geos
  for (list<geo*>::iterator i=m_geos.begin(); i!=m_geos.end(); i++) {
    (*i)->render();
  }
  glPopMatrix();

  // render the screen
  m_current_screen->render();

  timeval this_time;
  // stop valgrind complaining
  this_time.tv_sec=0;
  this_time.tv_usec=0;

  gettimeofday(&this_time,NULL);
  m_delta=(this_time.tv_sec-m_last_time.tv_sec)+
    (this_time.tv_usec-m_last_time.tv_usec)*0.000001f;

  if (m_delta<m_deadline) {
    //min 1 hz
    if(m_deadline-m_delta<1.0f)
      {
	usleep((int)((m_deadline-m_delta)*1000000.0f));
      }
  }

  m_last_time=this_time;
  //if (m_delta>0) m_time=this_time.tv_sec+this_time.tv_usec*0.000001f;
  if (m_delta>0.0f && m_delta<100.0f) m_time+=m_delta;

}

bool intro::finished()
{
  bool temp=m_finished;
  m_finished=false;
  return temp;
}

void intro::init_audio()
{
  feeble::samplerate=22050;
  feeble::time_length=5;
  feeble::value volvalue(0.1);
  feeble::mult volmult;

  // menu sound
  {
    feeble::time_length=0.1;
    feeble::envelope env(feeble::envelope::RAMPUP);
    feeble::mult envmult;
    feeble::value envmultvalue(3000);
    feeble::generator osc(feeble::generator::SINE);

    envmult.patch("a",&envmultvalue);
    envmult.patch("b",&env);
    osc.patch("freq",&envmult);
    volmult.patch("a",&osc);
    volmult.patch("b",&volvalue);
    volmult.execute();

    m_menu_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
  }

  // menu sound
  {
    feeble::time_length=0.1;
    feeble::envelope env(feeble::envelope::RAMPDOWN);
    feeble::mult envmult;
    feeble::value envmultvalue(6000);
    feeble::generator osc(feeble::generator::SINE);

    envmult.patch("a",&envmultvalue);
    envmult.patch("b",&env);
    osc.patch("freq",&envmult);
    volmult.patch("a",&osc);
    volmult.patch("b",&volvalue);
    volmult.execute();

    m_select_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
  }

  // bg sound
  {
    feeble::time_length=5;
    feeble::envelope env(feeble::envelope::HILL);
    feeble::mult envmult;
    feeble::value envmultvalue(100);
    feeble::generator master(feeble::generator::SINE);
    feeble::value multvalue(400);
    feeble::add add;
    feeble::value addvalue(10);
    feeble::mult mult;
    feeble::generator slave(feeble::generator::SINE);

    envmult.patch("a",&envmultvalue);
    envmult.patch("b",&env);
    master.patch("freq",&envmult);
    mult.patch("a",&master);
    mult.patch("b",&multvalue);
    add.patch("a",&mult);
    add.patch("b",&addvalue);
    slave.patch("freq",&add);
    volmult.patch("a",&slave);
    volmult.patch("b",&volvalue);
    volmult.execute();

    m_BGsound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
  }

  // bg2 sound
  {
    feeble::time_length=2;
    feeble::envelope env(feeble::envelope::RAMPDOWN);
    feeble::mult envmult;
    feeble::value envmultvalue(1000);
    feeble::generator master(feeble::generator::SINE);
    feeble::value multvalue(100);
    feeble::add add;
    feeble::value addvalue(400);
    feeble::mult mult;
    feeble::generator slave(feeble::generator::SINE);

    envmult.patch("a",&envmultvalue);
    envmult.patch("b",&env);
    master.patch("freq",&envmult);
    mult.patch("a",&master);
    mult.patch("b",&multvalue);
    add.patch("a",&mult);
    add.patch("b",&addvalue);
    slave.patch("freq",&add);
    volmult.patch("a",&slave);
    volmult.patch("b",&volvalue);
    volmult.execute();

    m_BG2sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
  }

}

/////////////////////////////////////////////////////////////////////

screen::screen(geo_game *game) :
  m_game(game),
  m_finished(false)
{
}

void screen::handle(unsigned char key, int button, int state, int x, int y)
{
  for (list<screen::widget*>::iterator i=m_widgets.begin(); i!=m_widgets.end(); i++)
    {
      if ((*i)->handle(key,button,state,x,y))
	{
	  if ((*i)->activated())
	    {
	      m_token=(*i)->get_token();
	    }
	  return;
	}
    }
}

void screen::update()
{
  for (list<screen::widget*>::iterator i=m_widgets.begin(); i!=m_widgets.end(); i++)
    {
      (*i)->update();
    }
}

void screen::render()
{
  glColor3f(1,1,1);
  glPushMatrix();
  glTranslatef(-6,5,0);
  glScalef(3,3,3);
  glLineWidth(3);
  draw_text(m_title);
  glLineWidth(1);
  glPopMatrix();

  for (list<screen::widget*>::iterator i=m_widgets.begin(); i!=m_widgets.end(); i++)
    {
      (*i)->render();
    }

}

bool screen::result(string& result)
{
  result=m_token;
  m_token="";
  bool temp=m_finished;
  m_finished=false;
  return temp;
}

string get_string(istringstream &s)
{
  // ignore space and [
  s.ignore(2);
  char buf[4096];
  s.getline(buf,4096,']');
  return buf;
}

void screen::build(istringstream &s)
{
  button* firstbutton=NULL;
  button* lastbutton=NULL;
  float ypos=0;
  while (!s.eof())
    {
      // get type token
      string type;
      s>>type;

      if (type=="title")
	{
	  m_title=get_string(s);
	}
      else if (type=="button")
	{
	  string name=get_string(s);
	  string token=get_string(s);
	  button* newbutton=new button(name,token,0,ypos++);
	  if (!lastbutton) firstbutton=newbutton;
	  else
	    {
	      newbutton->previous(lastbutton);
	      lastbutton->next(newbutton);
	    }
	  lastbutton=newbutton;
	  m_widgets.push_back(newbutton);
	}
      else if (type=="text")
	{
	  string t=get_string(s);
	  m_widgets.push_back(new text(t,0,ypos));
	  ypos+=0.5;
	}
      else if (type=="gap")
	{
	  ypos+=0.5;
	}
    }

  if (firstbutton)
    {
      firstbutton->previous(lastbutton);
      lastbutton->next(firstbutton);
      firstbutton->select();
    }
}

/////////////////////////////////////////////////////////////////

screen::button::button(const string &name, const string &token, float x, float y) :
  m_name(name),
  m_X(x),
  m_Y(y),
  m_selected(false),
  m_activated(false),
  m_previous(NULL),
  m_next(NULL),
  m_token(token)
{
}

bool screen::button::activated()
{
  bool temp=m_activated;
  m_activated=false;
  return temp;
}

bool screen::button::handle(unsigned char key, int button, int state, int x, int y)
{
  if (m_selected && state==0)
    {
      if (key==GLUT_KEY_UP && m_previous!=NULL)
	{
	  bleep::get()->play(intro::m_menu_sound, 220, 1);
	  m_selected=false;
	  m_previous->select();
	  return true;
	}

      if (key==GLUT_KEY_DOWN && m_next!=NULL)
	{
	  bleep::get()->play(intro::m_menu_sound, 220, 1);
	  m_selected=false;
	  m_next->select();
	  return true;
	}

      if (key==13)
	{
	  bleep::get()->play(intro::m_select_sound, 220, 1);
	  m_activated=true;
	  return true;
	}
    }

  return false;
}

void screen::button::update()
{
}

void screen::button::render()
{
  if (m_selected)
    {
      glColor4f(1,1,1,0.4);
      for (int n=0; n<3; n++)
	{
	  glPushMatrix();
	  glScalef(rand_range(0.99,1.01),rand_range(0.99,1.01),1);
	  glTranslatef(m_X-5,-m_Y+3,0);
	  draw_text(m_name);
	  glPopMatrix();
	}
    }
  else
    {
      glColor3f(1,1,1);
      glPushMatrix();
      glTranslatef(m_X-5,-m_Y+3,0);
      draw_text(m_name);
      glPopMatrix();
    }
}

/////////////////////////////////////////////////////////////////////////////

screen::text::text(const string &text, float x, float y) :
  m_X(x),
  m_Y(y),
  m_text(text)
{
}

void screen::text::render()
{
  glColor3f(1,1,1);
  glPushMatrix();
  glTranslatef(m_X-5,-m_Y+3,0);
  draw_text(m_text);
  glPopMatrix();
}
