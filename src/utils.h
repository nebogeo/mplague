/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <string>

using namespace std;

namespace geogame {

void draw_text(const string &text);

class primitive
{
public:
	static primitive *get();
	~primitive();

	void circle()     { glCallList(m_circle); }
	void triangle()     { glCallList(m_triangle); }
	void square()       { glCallList(m_square); }
	void pentagon()     { glCallList(m_pentagon); }
	void key()          { glCallList(m_key); }
	void scanner_key()   { glCallList(m_scanner_key); }
	void explosion()    { glCallList(m_explosion); }
	void particle()     { glCallList(m_particle); }
	void weapon_pickup() { glCallList(m_weapon_pickup); }

private:
	primitive();

	int m_circle;
	int m_triangle;
	int m_square;
	int m_pentagon;
	int m_key;
	int m_explosion;
	int m_particle;
	int m_scanner_key;
	int m_weapon_pickup;

	static primitive *m_singleton;

};

}
