/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "dada.h"
#include "entity.h"
#include <string>
#include <vector>
#include <list>

#ifndef GEOSTRUCTURE
#define GEOSTRUCTURE

namespace geogame {

  class structure : public entity {
  public:
    structure(d_vector pos, d_vector dir, float mass);
  
    virtual void update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist);
    virtual void render();
    void set_velocity(d_vector v) { m_velocity=v; };
    virtual float get_mass() { return m_mass; }
    virtual bool check_collide(const d_vector &pos, float radius, bool damage);

    void connect(structure *other) {
      float distance=m_position.dist(other->get_position());
      //other->m_connections.push_back(connection(this,distance));
      m_connections.push_back(connection(other,distance));
    }
    
  protected:
    class connection {
    public:
    connection(structure *other, float distance) :
      m_distance(distance), m_other(other), m_broken(false) {}
      float m_distance;
      structure *m_other;
      bool m_broken;
    };

    list<connection> m_connections;
    
  private:
    float m_mass;
    float m_size;
    d_vector m_spring;
  };

}

#endif

