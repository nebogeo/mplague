/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "dada.h"
#include "entity.h"
#include "orbital.h"
#include <string>
#include <list>
#include <vector>

#ifndef GEOGNAT
#define GEOGNAT

// gnat
// propellent
// - used by firing engine
// - regenerated over time (near big planets/stars)
// engine
// - fires in thrust direction
// - uses up propellent as mag() thrust
// - fires for a given time

namespace geogame {

  class planet;
  
  class gnat : public entity {
  public:
    gnat(d_vector pos, d_vector dir);
  
    virtual void update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist);
    virtual void render();
    virtual float get_mass() { return MIN_MASS; }
    virtual bool check_collide(const d_vector &pos, float radius, bool damage);

    enum gnat_state {
      GNAT_IDLE,
      GNAT_SEARCHING,
      GNAT_TRANSIT_BURN,
      GNAT_TRANSIT,
      GNAT_APPROACH,
      GNAT_FINAL_BURN,
      GNAT_FINAL,
      GNAT_PARKED,
      GNAT_TRANSPORT,
      GNAT_TRANSPORT_END,
    };

    string gnat_state_text(gnat_state s) {
      switch (s) {
      case GNAT_IDLE: return "IDLE";
      case GNAT_SEARCHING: return "SEARCHING";
      case GNAT_TRANSIT_BURN: return "TRANSIT_BURN";
      case GNAT_TRANSIT: return "TRANSIT";
      case GNAT_APPROACH: return "APPROACH";
      case GNAT_FINAL_BURN: return "FINAL_BURN";
      case GNAT_FINAL: return "FINAL";
      case GNAT_PARKED: return "PARKED";
      case GNAT_TRANSPORT: return "TRANSPORT";
      case GNAT_TRANSPORT_END: return "TRANSPORT_END";
      default: return "?";
      }
    }
    
  private:

    void log(const string &s);
    void thrust(const d_vector &thrust, list<entity_command> &cmdlist);
    void begin_delta_vee(const d_vector &desired);
    bool approachable(planet *p, entity *star);

    list<string> m_log;
    
    d_vector m_thrust;
    planet* m_target;
    gnat_state m_state;
    int m_timer;
    bool m_engine;
    int m_engine_time;
    float m_propellent;
    bool m_ahead;
    d_vector m_last_pos;
    unsigned int m_last_target;
  };

}

#endif

