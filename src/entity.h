/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <list>
#include "dada.h"

using namespace std;

#define ENTITYFLAG_ENEMY	       0x0001
#define ENTITYFLAG_PROJECTILE	       0x0002
#define ENTITYFLAG_EXPLOSION	       0x0004
#define ENTITYFLAG_KEY		       0x0008
#define ENTITYFLAG_CAUGHTKEY	       0x0010
#define ENTITYFLAG_PARTICLES	       0x0020
#define ENTITYFLAG_WEAPONPICKUP	       0x0040
#define ENTITYFLAG_CAUGHT_WEAPONPICKUP 0x0080
#define ENTITYFLAG_PLANET              0x0100
#define ENTITYFLAG_GNAT                0x0200
#define ENTITYFLAG_STRUCTURE           0x0400

#ifndef ENTITY
#define ENTITY

namespace geogame {

  class entity_command {
  public:
  entity_command(const string &t, void *d): type(t),data(d) {}
    string type;
    void *data;
  };

  class particles_command {
  public:
    d_vector pos;
    d_vector dir;
    d_vector vec;
  };
  
  class entity
  {
  public:
  entity() : m_id(m_next_id++), m_flags(0), m_remove_me(false) {}
  entity(d_vector pos)
    : m_id(m_next_id++),
      m_flags(0),
      m_remove_me(false),
      m_position(pos),
      m_last_position(pos) {}
  entity(d_vector pos, d_vector vel)
    : m_id(m_next_id++),
      m_flags(0),
      m_remove_me(false),
      m_position(pos),
      m_last_position(pos),
      m_velocity(vel)
      {}
    
    virtual ~entity() {}

    unsigned int get_id() { return m_id; }
    
    virtual void update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist);
    virtual void render()=0;
    virtual float get_mass() { return 0; }
    virtual bool check_collide(const d_vector &pos, float radius, bool damage=false)=0;

    const d_vector &get_position() { return m_last_position; }
    const d_vector &get_velocity() { return m_velocity; }

    virtual void post_update();

    bool remove_me() { return m_remove_me; }
    const list<entity*> &get_new_entities() { return m_new_entities; }
    const int get_flags() { return m_flags; }
    const d_bounding_box& get_bounding_box() { return m_bounding_box; }
    const d_vector gravity(const list<entity*> &world);
    
  protected:
    static unsigned int m_next_id;
    
    unsigned int m_id;
    d_bounding_box m_bounding_box;
    int m_flags;
    bool m_remove_me;

    d_vector m_position;
    d_vector m_last_position;
    d_vector m_velocity;

    list<entity*> m_new_entities;
  };

}
#endif
