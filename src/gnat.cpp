/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "gnat.h"
#include "planet.h"
#include "glinc.h"
#include "utils.h"
#include "orbital.h"

#include <iostream>

#define THRUST 0.001
#define ORBIT_ERROR 2
#define VELOCITY_MATCH 0.1
#define ORBIT_OFFSET 20
#define FINAL_DISTANCE 40
#define PLANET_DISTANCE 5
#define TRANSPORT_DISTANCE 3
#define TRANSPORT_ALTITUDE 500

namespace geogame {

  gnat::gnat(d_vector pos, d_vector dir) :
    entity(pos,dir),
    m_target(NULL),
    m_state(GNAT_SEARCHING),
    m_timer(0),
    m_engine_time(0),
    m_propellent(10),
    m_ahead(false)
  {
    m_flags=ENTITYFLAG_GNAT;
  }

  void gnat::log(const string &s) {
    m_log.push_back(s);
    if (m_log.size()>5) {
      m_log.pop_front();
    }
  }
  
  void gnat::thrust(const d_vector &thrust, list<entity_command> &cmdlist) {
    m_velocity+=thrust;
    particles_command *pc = new particles_command;
    pc->pos=m_position;
    pc->dir=thrust*100;
    pc->vec=m_velocity;	  
    cmdlist.push_back(entity_command("particles",(void*)pc));
  }	  

  void gnat::begin_delta_vee(const d_vector &desired) {
    d_vector delta=desired-m_velocity;
    d_vector dn = delta;
    dn.normalise();
    m_thrust=dn*THRUST;
    m_engine_time=delta.mag()/THRUST;
  }

  bool gnat::approachable(planet *p, entity *star) {
    float p_altitude=p->get_position().dist(star->get_position());
    // only small things with roughly circular orbits
    if (p->get_mass()<1.0 &&
	fabs(p->get_velocity().mag()-orbital::orbital_speed(p_altitude,star->get_mass())) < 1.0 &&
	// and below our target altitude for the ring
	p_altitude<TRANSPORT_ALTITUDE-ORBIT_ERROR
	) {
      return true;
    } else {
      return false;
    }
  }
  
  void gnat::update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist) {
    entity *star = *world.begin();
    float my_altitude=m_position.dist(star->get_position());
    float target_altitude = 0;
    float target_orbit_vel = 0;
    d_vector star_vec = m_position-star->get_position();
    star_vec.normalise();
    d_vector approach_orbit_vec(0,0,0);

    if (m_target && m_target->remove_me()) {
      m_target=NULL;
      m_state=GNAT_IDLE;
    }

    if (m_target) {
      if (m_state!=GNAT_TRANSPORT &&
	  m_state!=GNAT_TRANSPORT_END &&
	  !approachable(m_target,star)) {
	m_target=NULL;
	m_state=GNAT_IDLE;
      } else {
	target_altitude = m_target->get_position().dist(star->get_position());

	// is target ahead or behind?
	d_vector comp(-star_vec.y,star_vec.x,0);
	d_vector target_angle(m_target->get_position()-star->get_position());
	target_angle.normalise();
	if (comp.dot(target_angle)<0) {	
	  target_altitude+=ORBIT_OFFSET;
	  m_ahead=true;
	} else {
	  target_altitude-=ORBIT_OFFSET;
	  m_ahead=false;
	}
	
	target_orbit_vel = orbital::orbital_speed(target_altitude,star->get_mass());
	approach_orbit_vec.x=-star_vec.y;
	approach_orbit_vec.y=star_vec.x;
	approach_orbit_vec*=target_orbit_vel;	 
      }
    }
    
    switch (m_state) {

    case GNAT_IDLE: {
      if (m_timer>1000 && rand_range(0,1000)<2) {
	m_state=GNAT_SEARCHING;
      }
    } break;
      
    case GNAT_SEARCHING: {
      // search for a planetessimal
      float closest=99999;
      if (m_target) m_last_target=m_target->get_id();
      for (list<entity*>::const_iterator i = world.begin();
	   i!=world.end(); ++i) {
	if ((*i)->get_flags()==ENTITYFLAG_PLANET) {
	  planet *p = (planet*)*i;
	  if (approachable(p,star)) {
	    //float dist = fabs((p->get_position()-star->get_position()).mag()-
	    //		      my_altitude);
	    float dist = p->get_position().dist(m_position);
	    if (dist<closest && m_last_target!=(*i)->get_id()) {
	      m_target=p;
	      m_state=GNAT_TRANSIT_BURN;
	      closest=dist;
	    }
	  }
	}
      }
      
      //list<entity*>::const_iterator i = world.begin();
      //advance(i,6*1+17);
      //advance(i,1);
      //m_target=*i;      
      //m_state=GNAT_TRANSIT_BURN;
      
    } break;

    case GNAT_TRANSIT_BURN: {
      d_vector orbit(-star_vec.y,star_vec.x,0);
      orbit.normalise();
      float orbit_speed=orbital::orbital_speed(my_altitude,star->get_mass());
      // if higher, we need to slow down - otherwise vice versa
      orbit_speed*=(target_altitude/my_altitude);
      begin_delta_vee(orbit*orbit_speed);  
      log("transit burn for "+to_string(m_engine_time));      
      m_state=GNAT_TRANSIT;
      m_timer=0;
    } break;

    case GNAT_TRANSIT: {
      if (fabs(my_altitude-target_altitude)<ORBIT_ERROR) {
	begin_delta_vee(approach_orbit_vec);  
	m_state=GNAT_APPROACH;
      }

      if (m_timer>1000) {
	log("transit correction");
	m_state=GNAT_TRANSIT_BURN;
	m_timer=100;
      }
    } break;
      
    case GNAT_APPROACH: {
      if (fabs(my_altitude-target_altitude)>ORBIT_ERROR) {
	begin_delta_vee(approach_orbit_vec);  
	log("recorrect from helio orbit");
      } 	
      if (fabs(my_altitude-target_altitude)>ORBIT_ERROR*2) {
	log("helio orbit abort");
	m_state=GNAT_TRANSIT_BURN;
      } 	
      if (m_position.dist(m_target->get_position())<FINAL_DISTANCE) {
	m_state=GNAT_FINAL_BURN;
      }
    } break;
      
    case GNAT_FINAL_BURN: {
      d_vector delta(m_target->get_position()-m_position);
      delta.normalise();
      delta*=0.1;
      delta+=m_target->get_velocity()-m_velocity;
      delta*=0.5;
      d_vector dn(delta);
      dn.normalise();
      m_thrust=dn*THRUST;
      m_engine_time=delta.mag()/THRUST;
      //begin_delta_vee(//m_target->get_velocity()+
      //app*0.01);  
      log("final correction burn for "+to_string(m_engine_time));
      m_timer=0;
      m_state=GNAT_FINAL;
    } break;
      
    case GNAT_FINAL: {
      // drifted, restart 
      if (m_position.dist(m_target->get_position())>FINAL_DISTANCE) {
        log("restart h orbit from final");
	m_state=GNAT_APPROACH;
      }

      if (m_timer>100) {
	d_vector last_target_rel(m_last_pos-m_target->get_position());
	d_vector target_rel(m_position-m_target->get_position());
	if (last_target_rel.mag()<target_rel.mag()) {
	  // getting further away
	  log("getting further away, trying final again");
	  m_state=GNAT_FINAL_BURN;
	}
      }
      
      if (m_position.dist(m_target->get_position())<PLANET_DISTANCE &&
	  m_velocity.dist(m_target->get_velocity())<VELOCITY_MATCH) {
	log("reached correct planetary orbit");

	// break
	begin_delta_vee(m_target->get_velocity());
	log("breaking burn for "+to_string(m_engine_time));
	m_timer=0;
	m_state=GNAT_PARKED;
      }
    } break;
      
    case GNAT_PARKED: {
      if (m_position.dist(m_target->get_position()) > PLANET_DISTANCE*2) {
	log("planetary correction needed");
	m_state=GNAT_FINAL_BURN;
      }
      if (m_position.dist(m_target->get_position()) < TRANSPORT_DISTANCE) {
	log("transporting...");
	m_state=GNAT_TRANSPORT;
	begin_delta_vee(m_target->get_velocity()*2);      	
	m_timer=0;
      }
    } break;

    case GNAT_TRANSPORT: {
      if (my_altitude>TRANSPORT_ALTITUDE-ORBIT_ERROR) {
	float transport_orbit_vel = orbital::orbital_speed(TRANSPORT_ALTITUDE,star->get_mass());
	d_vector transport_orbit_vec(-star_vec.y,star_vec.x,0);
	transport_orbit_vec*=target_orbit_vel;	 
	begin_delta_vee(transport_orbit_vec);      	
	m_timer=0;
	m_state=GNAT_TRANSPORT_END;
      }
    } break;

    case GNAT_TRANSPORT_END: {
      if (m_engine_time==0) {
	// push in towards star
	d_vector delta(-m_position);
	delta.normalise();
	delta*=0.1;
	m_thrust=delta*THRUST;
	m_engine_time=1/THRUST;
	m_state=GNAT_IDLE;
      }
    } break;
      
    };

    m_velocity+=gravity(world);

    if (m_engine_time>0 && m_propellent>0) {
      if (m_state!=GNAT_TRANSPORT &&
	  m_state!=GNAT_TRANSPORT_END) {
	thrust(m_thrust,cmdlist);
      } else {
	thrust(m_thrust,cmdlist);
	m_target->set_velocity(m_target->get_velocity()+m_thrust);
	m_velocity=m_target->get_velocity();
      }
      m_propellent -= m_thrust.mag();
      m_engine_time--;           
    }

    // turn the engine off if we've run out of propellent
    if (m_propellent<0) m_engine_time=0;
        
    m_last_pos=m_position;
    m_position+=m_velocity;
    m_timer++;
    m_propellent+=0.001;
  }

  void gnat::render() {
    glColor4f(1.0,0,1.0,1.0);
    glLineWidth(1);
    glPushMatrix();
    glTranslatef(m_position.x,m_position.y,m_position.z);

    switch (m_state) {
    case GNAT_SEARCHING: glColor4f(1,1,1,1); break;
    case GNAT_TRANSIT_BURN: glColor4f(1,0,0,1);  break;
    case GNAT_TRANSIT: glColor4f(1,0,0,1); break;
    case GNAT_APPROACH: glColor4f(1,0.5,0,1); break;
    case GNAT_FINAL_BURN: glColor4f(0,1,0,1); break;
    case GNAT_FINAL: glColor4f(0,1,0,1); break;
    case GNAT_PARKED: glColor4f(0,0,1,1); break;
    }
    primitive::get()->pentagon();
    
    //glTranslatef(2.0,-6.5,0);
    /*    char text[256];
    glPushMatrix();
    glScalef(2,2,2);
    glTranslatef(0,-0.5,0);
    sprintf(text,"%d:%s p:%d t:%d ah:%d",m_id,
	    gnat_state_text(m_state).c_str(),
	    (int)m_propellent,m_engine_time,(int)m_ahead);
    draw_text(text);
    for(list<string>::iterator i=m_log.begin(); i!=m_log.end(); ++i) {
      glTranslatef(0,-0.5,0);
      sprintf(text,"%s",i->c_str());
      draw_text(text);
    }
    glPopMatrix();

    if (m_target!=NULL) {
      glColor4f(1,1,1,0.1);
      glBegin(GL_LINE_STRIP);
      glVertex3f(0,0,0);
      glVertex3f(m_target->get_position().x-m_position.x,
		 m_target->get_position().y-m_position.y,0);
	
      glEnd();
    }
    */
    glPopMatrix();
    glLineWidth(1);
  }

  bool gnat::check_collide(const d_vector &pos, float radius, bool damage) {
    return false;
  }

}
