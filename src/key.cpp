/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "glinc.h"
#include "key.h"
#include "bleep.h"
#include "utils.h"

using namespace geogame;

static const float MAX_GRAV_DIST=80;

pickup::pickup(const d_vector &position, float lag) :
  entity(position),
  m_max_speed(lag),
  m_max_speed_sq(m_max_speed*m_max_speed),
  m_following_mode(false),
  m_follow_dist_sq(1)
{
}

pickup::~pickup()
{
}

void pickup::update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist)
{
  if (!m_following_mode && playerpos.distsq(m_position)<m_follow_dist_sq)
    {
      bleep::get()->play(m_capure_sound, 220+rand_range(-10,10), 1);
      m_following_mode=true;
      m_flags|=ENTITYFLAG_CAUGHTKEY;
    }

  if (m_following_mode) // just follow the player around...
    {
      m_velocity+=(playerpos-m_position)*0.01;
      m_velocity/=m_max_speed*3;
    }
  else
    {
      d_vector gravity;
      for (list<entity*>::const_iterator i=world.begin(); i!=world.end(); i++)
	{
	  if ((*i)->get_mass())
	    {
	      d_vector bet=(*i)->get_position()-m_position;
	      float distance = bet.mag();
	      bet.normalise();
	      if (distance<MAX_GRAV_DIST)
		{
		  gravity+=bet*(1-(distance/MAX_GRAV_DIST))*((*i)->get_mass()*0.000001);
		}
	    }
	  m_velocity+=gravity;
	}

    }

  if (m_velocity.magsq()>m_max_speed_sq)
    {
      m_velocity.normalise();
      m_velocity*=m_max_speed;
    }

  m_position+=m_velocity;
  if (m_position.x<-200) m_position.x+=400;
  if (m_position.x>200) m_position.x-=400;
  if (m_position.y<-200) m_position.y+=400;
  if (m_position.y>200) m_position.y-=400;

}

void pickup::render()
{
}

bool pickup::check_collide(const d_vector &pos, float radius, bool damage)
{
  return false;
}

////////////////////////////////////////////////////////////////////////

key::key(const d_vector &position, float lag) :
  pickup(position,lag)
{
  m_flags|=ENTITYFLAG_KEY;
}

key::~key()
{
}

void key::update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist)
{
  if (!m_following_mode && playerpos.distsq(m_position)<m_follow_dist_sq)
    {
      bleep::get()->play(m_capure_sound, 220+rand_range(-10,10), 1);
      m_following_mode=true;
      m_flags|=ENTITYFLAG_CAUGHTKEY;
    }

  pickup::update(world,playerpos,cmdlist);
}

void key::render()
{
  if (m_following_mode) glColor4f(1,0,0.5,0.5);
  else glColor4f(1,0.5,0,0.5);
  glLineWidth(2);
  glPushMatrix();
  glTranslatef(m_position.x,m_position.y,m_position.z);
  glRotatef(-atan(m_velocity.x/m_velocity.y)*RAD_CONV,0,0,1);
  primitive::get()->key();
  glPopMatrix();
  glLineWidth(1);
}

////////////////////////////////////////////////////////////////////////

weapon_pickup::weapon_pickup(const d_vector &position, float lag) :
  pickup(position,lag)
{
  m_follow_dist_sq=0.25;
  m_flags|=ENTITYFLAG_WEAPONPICKUP;
}

weapon_pickup::~weapon_pickup()
{
}

void weapon_pickup::update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist)
{
  if (!m_following_mode && playerpos.distsq(m_position)<m_follow_dist_sq)
    {
      bleep::get()->play(m_capure_sound, 220+rand_range(-10,10), 1);
      m_flags|=ENTITYFLAG_CAUGHT_WEAPONPICKUP;
      m_remove_me=true;
    }

  pickup::update(world,playerpos,cmdlist);
}

void weapon_pickup::render()
{
  if (m_following_mode) glColor4f(1,0,0.5,0.5);
  else glColor4f(1,0.5,0,0.5);
  glLineWidth(2);
  glPushMatrix();
  glTranslatef(m_position.x,m_position.y,m_position.z);
  glRotatef(-atan(m_velocity.x/m_velocity.y)*RAD_CONV,0,0,1);
  primitive::get()->weapon_pickup();
  glPopMatrix();
  glLineWidth(1);
}
