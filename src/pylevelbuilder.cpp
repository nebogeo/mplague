/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "pylevelbuilder.h"
#include <iostream>

using namespace std;

PyLevelBuilder *PyLevelBuilder::m_Singleton=NULL;
PyMethodDef PyLevelBuilder::m_Methods[256];
dMatrix PyLevelBuilder::m_Space;

static PyMethodDef methods[] =
{
	{"translate", PyLevelBuilder::Translate, 1,""},
	{"rotate", PyLevelBuilder::Rotate, 1,""},
	{"scale", PyLevelBuilder::Scale, 1,""},
	{NULL, NULL} /* sentinel */
};

PyLevelBuilder::PyLevelBuilder() :
m_Game(NULL),
m_BindingCount(0)
{
	Py_Initialize();
	PyImport_AddModule("levelbuilder");
	Py_InitModule("levelbuilder", methods);
}

PyLevelBuilder::~PyLevelBuilder()
{
	// Exit, cleaning up the interpreter
	Py_Exit(0);
}

PyLevelBuilder *PyLevelBuilder::Get()
{
	if (!m_Singleton)
	{
		m_Singleton=new PyLevelBuilder;
	}
	return m_Singleton;
}

void PyLevelBuilder::Source(const string &filename)
{
	FILE *fd=fopen(filename.c_str(),"r");
	if (fd)
	{
		fseek(fd,0,SEEK_END);
		int size=ftell(fd);
		fseek(fd,0,SEEK_SET);
		char *script = new char[size];
		fread(script,1,size,fd);
		fclose(fd);
		script[size]='\0';
		PyRun_SimpleString(script);
	}
}

void PyLevelBuilder::DoString(const string &script)
{
	PyRun_SimpleString(script.c_str());
}

/////////////////////////////////////////////////////////////////////

PyObject *PyLevelBuilder::Translate(PyObject *self, PyObject* args)
{
	float v[3];
	if (!PyArg_ParseTuple(args, "(fff)", &v[0], &v[1], &v[2])) return NULL;
    m_Space.translate(v[0],v[1],v[2]);
    Py_INCREF(Py_None);
	return Py_None;
}

PyObject *PyLevelBuilder::Rotate(PyObject *self, PyObject* args)
{
	float v[3];
	if (!PyArg_ParseTuple(args, "(fff)", &v[0], &v[1], &v[2])) return NULL;
    m_Space.rotxyz(v[0],v[1],v[2]);
    Py_INCREF(Py_None);
	return Py_None;
}

PyObject *PyLevelBuilder::Scale(PyObject *self, PyObject* args)
{
	float v[3];
	if (!PyArg_ParseTuple(args, "(fff)", &v[0], &v[1], &v[2])) return NULL;
    m_Space.scale(v[0],v[1],v[2]);
    Py_INCREF(Py_None);
	return Py_None;
}

PyObject *NewGeo(PyObject *self, PyObject* args)
{
	if (!PyArg_ParseTuple(args, "(fff)", &v[0], &v[1], &v[2])) return NULL;
}
