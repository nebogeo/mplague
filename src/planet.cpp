/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "planet.h"
#include "glinc.h"
#include "utils.h"

namespace geogame {

  planet::planet(d_vector pos, d_vector dir, float mass, d_vector col, float size) :
    entity(pos,dir),
    m_col(col),
    m_mass(mass),
    m_size(size)
  {
    m_flags=ENTITYFLAG_PLANET;
  }
  
  void planet::update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist) {
    
    m_velocity+=gravity(world);
    
    if (m_velocity.magsq()>1) {
       m_velocity.normalise();
       m_velocity*=1;
    }
    
    m_position+=m_velocity;
    // if (m_position.x<-2000) m_position.x+=400;
    // if (m_position.x>2000) m_position.x-=400;
    // if (m_position.y<-2000) m_position.y+=400;
    // if (m_position.y>2000) m_position.y-=400;

  }

  void planet::render() {
    glColor4f(m_col.x,m_col.y,m_col.z,1.0);
    glLineWidth(1);
    glPushMatrix();
    glTranslatef(m_position.x,m_position.y,m_position.z);
    glScalef(m_size,m_size,m_size);
    primitive::get()->circle();
    glPopMatrix();
    glLineWidth(1);
  }

  bool planet::check_collide(const d_vector &pos, float radius, bool damage) {
    return false;
  }

}
