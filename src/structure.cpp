/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "structure.h"
#include "glinc.h"
#include "utils.h"

namespace geogame {

  structure::structure(d_vector pos, d_vector dir, float mass) :
    entity(pos,dir),
    m_mass(mass),
    m_spring(0,0,0)
  {
    m_flags=ENTITYFLAG_STRUCTURE;
  }
  
  void structure::update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist) {

    d_vector newspring(0,0,0);
    for(list<connection>::iterator i=m_connections.begin();
	i!=m_connections.end(); i++) {
      d_vector bet=i->m_other->get_position()-m_position;
      float force=bet.mag()-i->m_distance;
      //cerr<<bet.mag()<<" "<<i->m_distance<<" "<<force<<endl;
      if (force>2.3) {
	i->m_broken=true;
      }
      bet.normalise();
      newspring+=bet*force;
    }

    m_spring=(m_spring*0.9)+(newspring*0.1);
    m_position+=m_spring;

    m_spring*=0;

    
    list<connection>::iterator next=m_connections.begin();
    while(next!=m_connections.end()) {
      list<connection>::iterator i=next;
      next++;
      if (i->m_broken) m_connections.erase(i);
    }

    m_velocity+=gravity(world);    
    m_position+=m_velocity;
  }

  void structure::render() {
    glColor4f(0.7,0.7,0.0,1.0);
    glLineWidth(1);
    glPushMatrix();
    glTranslatef(m_position.x,m_position.y,m_position.z);
    glScalef(0.2,0.2,0.2);
    primitive::get()->circle();
    glPopMatrix();
    glLineWidth(1);

    glPointSize(1);	  
    glBegin(GL_LINES);
    float count=0;
    for(list<connection>::iterator i=m_connections.begin();
	i!=m_connections.end(); i++) {
      glVertex3f(m_position.x,m_position.y,m_position.z);
      glVertex3f(i->m_other->get_position().x,
		 i->m_other->get_position().y,0);
    }
    glEnd();
  }

  bool structure::check_collide(const d_vector &pos, float radius, bool damage) {
    return false;
  }

}
