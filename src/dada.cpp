/*  dada
 *  Copyright (C) 2015 david griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
*/
#include "dada.h"
#include "stdlib.h"

using namespace geogame;

static const int SINCOS_TABLESIZE = 2048;
static float sin_tab[SINCOS_TABLESIZE];
static float cos_tab[SINCOS_TABLESIZE];
static const float SINCOS_LOOKUP=SINCOS_TABLESIZE/(float)TWO_PI;

float geogame::rand_float()
{
  return rand()%10000/10000.0f;
}

float geogame::rand_range(float L, float H)
{
  return ((rand()%10000/10000.0f)*(H-L))+L;
}

void geogame::init_dada()
{
  for (int n=0; n<SINCOS_TABLESIZE; n++)
    {
      float a=n*(TWO_PI/(float)SINCOS_TABLESIZE);
      sin_tab[n]=sin(a);
      cos_tab[n]=cos(a);
    }
}

void geogame::d_sin_cos(float a, float &s, float &c)
{
#ifdef WIN32
  s=sin(a);
  c=cos(a);
#else
  int index=(int)rint(a*SINCOS_LOOKUP)&SINCOS_TABLESIZE-1;
  s=sin_tab[index];
  c=cos_tab[index];
#endif
}

d_vector &d_vector::operator=(d_vector const &rhs)
{
  x=rhs.x; y=rhs.y; z=rhs.z; w=rhs.w;
  return *this;
}

d_vector d_vector::operator+(d_vector const &rhs) const
{
  d_vector t;
  t.x=x+rhs.x; t.y=y+rhs.y; t.z=z+rhs.z; //t.w=w+rhs.w;
  return t;
}

d_vector d_vector::operator-(d_vector const &rhs) const
{
  d_vector t;
  t.x=x-rhs.x; t.y=y-rhs.y; t.z=z-rhs.z; //t.w=w-rhs.w;
  return t;
}

d_vector d_vector::operator*(float rhs) const
{
  d_vector t;
  t.x=x*rhs; t.y=y*rhs; t.z=z*rhs; //t.w=w*rhs;
  return t;
}

d_vector d_vector::operator/(float rhs) const
{
  d_vector t;
  t.x=x/rhs; t.y=y/rhs; t.z=z/rhs; //t.w=w/rhs;
  return t;
}

d_vector &d_vector::operator+=(d_vector const &rhs)
{
  x+=rhs.x; y+=rhs.y; z+=rhs.z; //w+=rhs.w;
  return *this;
}

d_vector &d_vector::operator-=(d_vector const &rhs)
{
  x-=rhs.x; y-=rhs.y; z-=rhs.z; //w-=rhs.w;
  return *this;
}

d_vector &d_vector::operator*=(float rhs)
{
  x*=rhs; y*=rhs; z*=rhs; //w*=rhs;
  return *this;
}

d_vector &d_vector::operator/=(float rhs)
{
  if (rhs) {x/=rhs; y/=rhs; z/=rhs;}// w/=rhs;}
  return *this;
}

float d_vector::dot(d_vector const &rhs) const
{
  return x*rhs.x+y*rhs.y+z*rhs.z;
}

d_vector d_vector::cross(d_vector const &rhs) const
{
  return d_vector(y*rhs.z - z*rhs.y,
		  z*rhs.x - x*rhs.z,
		  x*rhs.y - y*rhs.x);
}

float d_vector::dist(d_vector const &rhs) const
{
  return sqrt((rhs.x-x)*(rhs.x-x)+
	      (rhs.y-y)*(rhs.y-y)+
	      (rhs.z-z)*(rhs.z-z));
}

float d_vector::distsq(d_vector const &rhs) const
{
  return (rhs.x-x)*(rhs.x-x)+
    (rhs.y-y)*(rhs.y-y)+
    (rhs.z-z)*(rhs.z-z);
}

void d_vector::get_euler(float &rx, float &ry, float &rz) const
{
  if (z==0) rx=0;
  else rx=atan(y/z)*RAD_CONV;
  if (x==0) ry=0;
  else ry=atan(z/x)*RAD_CONV;
  if (y==0) rz=0;
  else rz=atan(x/y)*RAD_CONV;
}

float d_vector::mag() const
{
  //return dist(d_vector(0,0,0));
  return sqrt(x*x+y*y+z*z);
}

float d_vector::magsq() const
{
  return x*x+y*y+z*z;
}

d_vector geogame::operator-(d_vector rhs)
{
  return d_vector(-rhs.x,-rhs.y,-rhs.z);
}

ostream &geogame::operator<<(ostream &os, d_vector const &om)
{
  os<<om.x<<" "<<om.y<<" "<<om.z<<" "<<om.w<<" ";
  return os;
}

istream &geogame::operator>>(istream &is, d_vector &om)
{
  is>>om.x>>om.y>>om.z>>om.w;
  return is;
}

void d_vector::get_rot(float m[16],d_vector up)
{
  d_vector a,b,c;
  a.x=this->x; a.y=this->y; a.z=this->z;
  a.normalise();
  if (a==up) a.x+=0.01;
  b=a.cross(up);
  b.normalise();
  c=b.cross(a);
  c.normalise();

  for (int n=0; n<16; n++)
    m[n]=0;

  m[15]=1;

  m[0]=a.x; m[1]=a.y;	m[2]=a.z;
  m[4]=b.x; m[5]=b.y;	m[6]=b.z;
  m[8]=c.x; m[9]=c.y;	m[10]=c.z;
}

////

d_colour &d_colour::operator=(d_colour const &rhs)
{
  r=rhs.r; g=rhs.g; b=rhs.b; a=rhs.a;
  return *this;
}

d_colour d_colour::operator+(d_colour const &rhs)
{
  d_colour t;
  t.r=r+rhs.r; t.g=g+rhs.g; t.b=b+rhs.b; t.a=a+rhs.a;
  return t;
}

d_colour d_colour::operator-(d_colour const &rhs)
{
  d_colour t;
  t.r=r-rhs.r; t.g=g-rhs.g; t.b=b-rhs.b; t.a=a-rhs.a;
  return t;
}

d_colour d_colour::operator*(float rhs)
{
  d_colour t;
  t.r=r*rhs; t.g=g*rhs; t.b=b*rhs; t.a=a*rhs;
  return t;
}

d_colour d_colour::operator/(float rhs)
{
  d_colour t;
  t.r=r/rhs; t.g=g/rhs; t.b=b/rhs; t.a=a/rhs;
  return t;
}

d_colour &d_colour::operator+=(d_colour const &rhs)
{
  r+=rhs.r; g+=rhs.g; b+=rhs.b; a+=rhs.a;
  return *this;
}

d_colour &d_colour::operator-=(d_colour const &rhs)
{
  r-=rhs.r; g-=rhs.g; b-=rhs.b; a-=rhs.a;
  return *this;
}

d_colour &d_colour::operator*=(float rhs)
{
  r*=rhs; g*=rhs; b*=rhs; a*=rhs;
  return *this;
}

d_colour &d_colour::operator/=(float rhs)
{
  if (rhs) {r/=rhs; g/=rhs; b/=rhs; a/=rhs;}
  return *this;
}

ostream &geogame::operator<<(ostream &os, d_colour const &om)
{
  os<<"r="<<om.r<<" g="<<om.g<<" b="<<om.b<<" a="<<om.a<<" ";
  return os;
}

////

d_vertex const &d_vertex::operator=(d_vertex const &rhs)
{
  point=rhs.point;
  normal=rhs.normal;
  col=rhs.col;
  s=rhs.s;
  t=rhs.t;
  return rhs;
}

ostream &geogame::operator<<(ostream &os, d_vertex const &v)
{
  os<<"vertex : p="<<v.point<<" n="<<v.normal<<v.col<<" "<<v.s<<" "<<v.t<<endl;
  return os;
}

////

void d_matrix::init()
{
  m[0][0]=m[1][1]=m[2][2]=m[3][3]=1;
  m[0][1]=m[0][2]=m[0][3]=0;
  m[1][0]=m[1][2]=m[1][3]=0;
  m[2][0]=m[2][1]=m[2][3]=0;
  m[3][0]=m[3][1]=m[3][2]=0;
}

const d_matrix &d_matrix::operator=(d_matrix const &rhs)
{
  m[0][0]=rhs.m[0][0]; m[0][1]=rhs.m[0][1]; m[0][2]=rhs.m[0][2]; m[0][3]=rhs.m[0][3];
  m[1][0]=rhs.m[1][0]; m[1][1]=rhs.m[1][1]; m[1][2]=rhs.m[1][2]; m[1][3]=rhs.m[1][3];
  m[2][0]=rhs.m[2][0]; m[2][1]=rhs.m[2][1]; m[2][2]=rhs.m[2][2]; m[2][3]=rhs.m[2][3];
  m[3][0]=rhs.m[3][0]; m[3][1]=rhs.m[3][1]; m[3][2]=rhs.m[3][2]; m[3][3]=rhs.m[3][3];
  return rhs;
}

d_matrix d_matrix::operator+(d_matrix const &rhs)
{
  d_matrix t;
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      t.m[i][j]=m[i][j]+rhs.m[i][j];
  return t;
}

d_matrix d_matrix::operator-(d_matrix const &rhs)
{
  d_matrix t;
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      t.m[i][j]=m[i][j]-rhs.m[i][j];
  return t;
}

d_matrix d_matrix::operator*(d_matrix const &rhs)
{
  //d_matrix t;
  //for (int i=0; i<4; i++)
  //    for (int j=0; j<4; j++)
  //        t.m[i][j]=m[i][0]*rhs.m[0][j]+
  //                  m[i][1]*rhs.m[1][j]+
  //                  m[i][2]*rhs.m[2][j]+
  //                  m[i][3]*rhs.m[3][j];

  d_matrix t;
  /*for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
    t.m[i][j]=m[0][j]*rhs.m[i][0]+m[1][j]*rhs.m[i][1]+m[2][j]*rhs.m[i][2]+m[3][j]*rhs.m[i][3];
  */

  t.m[0][0]=m[0][0]*rhs.m[0][0]+m[1][0]*rhs.m[0][1]+m[2][0]*rhs.m[0][2]+m[3][0]*rhs.m[0][3];
  t.m[0][1]=m[0][1]*rhs.m[0][0]+m[1][1]*rhs.m[0][1]+m[2][1]*rhs.m[0][2]+m[3][1]*rhs.m[0][3];
  t.m[0][2]=m[0][2]*rhs.m[0][0]+m[1][2]*rhs.m[0][1]+m[2][2]*rhs.m[0][2]+m[3][2]*rhs.m[0][3];
  t.m[0][3]=m[0][3]*rhs.m[0][0]+m[1][3]*rhs.m[0][1]+m[2][3]*rhs.m[0][2]+m[3][3]*rhs.m[0][3];

  t.m[1][0]=m[0][0]*rhs.m[1][0]+m[1][0]*rhs.m[1][1]+m[2][0]*rhs.m[1][2]+m[3][0]*rhs.m[1][3];
  t.m[1][1]=m[0][1]*rhs.m[1][0]+m[1][1]*rhs.m[1][1]+m[2][1]*rhs.m[1][2]+m[3][1]*rhs.m[1][3];
  t.m[1][2]=m[0][2]*rhs.m[1][0]+m[1][2]*rhs.m[1][1]+m[2][2]*rhs.m[1][2]+m[3][2]*rhs.m[1][3];
  t.m[1][3]=m[0][3]*rhs.m[1][0]+m[1][3]*rhs.m[1][1]+m[2][3]*rhs.m[1][2]+m[3][3]*rhs.m[1][3];

  t.m[2][0]=m[0][0]*rhs.m[2][0]+m[1][0]*rhs.m[2][1]+m[2][0]*rhs.m[2][2]+m[3][0]*rhs.m[2][3];
  t.m[2][1]=m[0][1]*rhs.m[2][0]+m[1][1]*rhs.m[2][1]+m[2][1]*rhs.m[2][2]+m[3][1]*rhs.m[2][3];
  t.m[2][2]=m[0][2]*rhs.m[2][0]+m[1][2]*rhs.m[2][1]+m[2][2]*rhs.m[2][2]+m[3][2]*rhs.m[2][3];
  t.m[2][3]=m[0][3]*rhs.m[2][0]+m[1][3]*rhs.m[2][1]+m[2][3]*rhs.m[2][2]+m[3][3]*rhs.m[2][3];

  t.m[3][0]=m[0][0]*rhs.m[3][0]+m[1][0]*rhs.m[3][1]+m[2][0]*rhs.m[3][2]+m[3][0]*rhs.m[3][3];
  t.m[3][1]=m[0][1]*rhs.m[3][0]+m[1][1]*rhs.m[3][1]+m[2][1]*rhs.m[3][2]+m[3][1]*rhs.m[3][3];
  t.m[3][2]=m[0][2]*rhs.m[3][0]+m[1][2]*rhs.m[3][1]+m[2][2]*rhs.m[3][2]+m[3][2]*rhs.m[3][3];
  t.m[3][3]=m[0][3]*rhs.m[3][0]+m[1][3]*rhs.m[3][1]+m[2][3]*rhs.m[3][2]+m[3][3]*rhs.m[3][3];

  return t;
}

d_matrix d_matrix::operator/(d_matrix const &rhs)
{
  d_matrix t;
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      t.m[i][j]=m[i][0]/rhs.m[0][j]+
	m[i][1]/rhs.m[1][j]+
	m[i][2]/rhs.m[2][j]+
	m[i][3]/rhs.m[3][j];
  return t;
}

d_matrix &d_matrix::operator+=(d_matrix const &rhs)
{
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      m[i][j]+=rhs.m[i][j];
  return *this;
}

d_matrix &d_matrix::operator-=(d_matrix const &rhs)
{
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      m[i][j]-=rhs.m[i][j];
  return *this;
}

d_matrix &d_matrix::operator*=(d_matrix const &rhs)
{
  *this=*this*rhs;
  return *this;
}

d_matrix &d_matrix::operator/=(d_matrix const &rhs)
{
  *this=*this/rhs;
  return *this;
}

d_matrix &d_matrix::translate(float x, float y, float z)
{
  d_matrix t;
  t.m[3][0]=x;
  t.m[3][1]=y;
  t.m[3][2]=z;
  *this=*this*t;
  return *this;

}

d_matrix &d_matrix::translate(d_vector &tr)
{
  d_matrix t;
  t.m[3][0]=tr.x;
  t.m[3][1]=tr.y;
  t.m[3][2]=tr.z;
  *this=*this*t;
  return *this;

}

void d_matrix::settranslate(d_vector &tr)
{
  m[3][0]=tr.x;
  m[3][1]=tr.y;
  m[3][2]=tr.z;
}

d_vector d_matrix::gettranslate()
{
  return d_vector(m[3][0],m[3][1],m[3][2]);
}

#define USE_FAST_SINCOS

d_matrix &d_matrix::rotxyz(float x,float y,float z)
{
  d_matrix t;
  if (x)
    {
      x*=0.017453292;

#ifdef USE_FAST_SINCOS
      float sx,cx;
      d_sin_cos(x,sx,cx);
#else
      float sx=sin(x);
      float cx=cos(x);
#endif

      t.m[1][1]=cx;
      t.m[2][1]=-sx;
      t.m[1][2]=sx;
      t.m[2][2]=cx;
      *this=*this*t;
    }

  if (y)
    {
      y*=0.017453292;

#ifdef USE_FAST_SINCOS
      float sy,cy;
      d_sin_cos(y,sy,cy);
#else
      float sy=sin(y);
      float cy=cos(y);
#endif

      t.init();
      t.m[0][0]=cy;
      t.m[2][0]=-sy;
      t.m[0][2]=sy;
      t.m[2][2]=cy;
      *this=*this*t;
    }

  if (z)
    {
      z*=0.017453292;

#ifdef USE_FAST_SINCOS
      float sz,cz;
      d_sin_cos(z,sz,cz);
#else
      float sz=sin(z);
      float cz=cos(z);
#endif

      t.init();
      t.m[0][0]=cz;
      t.m[1][0]=-sz;
      t.m[0][1]=sz;
      t.m[1][1]=cz;
      *this=*this*t;
    }

  return *this;
}

d_matrix &d_matrix::rotx(float a)
{
  a*=0.017453292;
  d_matrix t;

  t.m[1][1]=cos(a);
  t.m[2][1]=-sin(a);
  t.m[1][2]=sin(a);
  t.m[2][2]=cos(a);

  *this=*this*t;
  return *this;
}

d_matrix &d_matrix::roty(float a)
{
  a*=0.017453292;
  d_matrix t;

  t.m[0][0]=cos(a);
  t.m[2][0]=-sin(a);
  t.m[0][2]=sin(a);
  t.m[2][2]=cos(a);

  *this=*this*t;
  return *this;
}

d_matrix &d_matrix::rotz(float a)
{
  a*=0.017453292;
  d_matrix t;

  t.m[0][0]=cos(a);
  t.m[1][0]=-sin(a);
  t.m[0][1]=sin(a);
  t.m[1][1]=cos(a);

  *this=*this*t;
  return *this;
}

d_matrix &d_matrix::scale(float x, float y, float z)
{
  d_matrix t;

  t.m[0][0]=x;
  t.m[1][1]=y;
  t.m[2][2]=z;

  *this=*this*t;
  return *this;
}

d_vector d_matrix::transform(d_vector const &p) const
{
  d_vector t;
  t.x=p.x*m[0][0] + p.y*m[1][0] + p.z*m[2][0] + p.w*m[3][0];
  t.y=p.x*m[0][1] + p.y*m[1][1] + p.z*m[2][1] + p.w*m[3][1];
  t.z=p.x*m[0][2] + p.y*m[1][2] + p.z*m[2][2] + p.w*m[3][2];
  t.w=p.x*m[0][3] + p.y*m[1][3] + p.z*m[2][3] + p.w*m[3][3];
  return t;
}

d_vertex d_matrix::transform(d_vertex const &p) const
{
  d_vertex t=p;
  t.point=transform(p.point);
  t.normal=transform_no_trans(p.normal);
  return t;
}

d_vector d_matrix::transform_no_trans(d_vector const &p) const
{
  d_vector t;
  t.x=p.x*m[0][0] + p.y*m[1][0] + p.z*m[2][0];
  t.y=p.x*m[0][1] + p.y*m[1][1] + p.z*m[2][1];
  t.z=p.x*m[0][2] + p.y*m[1][2] + p.z*m[2][2];
  t.w=p.w;
  return t;
}

/*void d_matrix::load_glmatrix(float glm[16])
  {
  glm[0]= m[0][0]; glm[1]= m[1][0]; glm[2]= m[2][0]; glm[3]= m[3][0];
  glm[4]= m[0][1]; glm[5]= m[1][1]; glm[6]= m[2][1]; glm[7]= m[3][1];
  glm[8]= m[0][2]; glm[9]= m[1][2]; glm[10]=m[2][2]; glm[11]=m[3][2];
  glm[12]=m[0][3]; glm[13]=m[1][3]; glm[14]=m[2][3]; glm[15]=m[3][3];
  }*/

void d_matrix::load_glmatrix(float glm[16])
{
  glm[0]= m[0][0]; glm[4]= m[1][0]; glm[8]= m[2][0]; glm[12]= m[3][0];
  glm[1]= m[0][1]; glm[5]= m[1][1]; glm[9]= m[2][1]; glm[13]= m[3][1];
  glm[2]= m[0][2]; glm[6]= m[1][2]; glm[10]=m[2][2]; glm[14]=m[3][2];
  glm[3]= m[0][3]; glm[7]= m[1][3]; glm[11]=m[2][3]; glm[15]=m[3][3];
}

void d_matrix::load_d_matrix(float glm[16])
{
  m[0][0]=glm[0]; m[1][0]=glm[4]; m[2][0]=glm[8]; m[3][0]=glm[12];
  m[0][1]=glm[1]; m[1][1]=glm[5]; m[2][1]=glm[9]; m[3][1]=glm[13];
  m[0][2]=glm[2]; m[1][2]=glm[6]; m[2][2]=glm[10]; m[3][2]=glm[14];
  m[0][3]=glm[3]; m[1][3]=glm[7]; m[2][3]=glm[11]; m[3][3]=glm[15];
}

void d_matrix::transpose()
{
  d_matrix t;
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      t.m[i][j]=m[j][i];
  *this=t;
}

d_matrix d_matrix::inverse()
{
  d_matrix temp;
  temp.m[0][0] = m[1][2]*m[2][3]*m[3][1] - m[1][3]*m[2][2]*m[3][1] + m[1][3]*m[2][1]*m[3][2] - m[1][1]*m[2][3]*m[3][2] - m[1][2]*m[2][1]*m[3][3] + m[1][1]*m[2][2]*m[3][3];
  temp.m[0][1] = m[0][3]*m[2][2]*m[3][1] - m[0][2]*m[2][3]*m[3][1] - m[0][3]*m[2][1]*m[3][2] + m[0][1]*m[2][3]*m[3][2] + m[0][2]*m[2][1]*m[3][3] - m[0][1]*m[2][2]*m[3][3];
  temp.m[0][2] = m[0][2]*m[1][3]*m[3][1] - m[0][3]*m[1][2]*m[3][1] + m[0][3]*m[1][1]*m[3][2] - m[0][1]*m[1][3]*m[3][2] - m[0][2]*m[1][1]*m[3][3] + m[0][1]*m[1][2]*m[3][3];
  temp.m[0][3] = m[0][3]*m[1][2]*m[2][1] - m[0][2]*m[1][3]*m[2][1] - m[0][3]*m[1][1]*m[2][2] + m[0][1]*m[1][3]*m[2][2] + m[0][2]*m[1][1]*m[2][3] - m[0][1]*m[1][2]*m[2][3];
  temp.m[1][0] = m[1][3]*m[2][2]*m[3][0] - m[1][2]*m[2][3]*m[3][0] - m[1][3]*m[2][0]*m[3][2] + m[1][0]*m[2][3]*m[3][2] + m[1][2]*m[2][0]*m[3][3] - m[1][0]*m[2][2]*m[3][3];
  temp.m[1][1] = m[0][2]*m[2][3]*m[3][0] - m[0][3]*m[2][2]*m[3][0] + m[0][3]*m[2][0]*m[3][2] - m[0][0]*m[2][3]*m[3][2] - m[0][2]*m[2][0]*m[3][3] + m[0][0]*m[2][2]*m[3][3];
  temp.m[1][2] = m[0][3]*m[1][2]*m[3][0] - m[0][2]*m[1][3]*m[3][0] - m[0][3]*m[1][0]*m[3][2] + m[0][0]*m[1][3]*m[3][2] + m[0][2]*m[1][0]*m[3][3] - m[0][0]*m[1][2]*m[3][3];
  temp.m[1][3] = m[0][2]*m[1][3]*m[2][0] - m[0][3]*m[1][2]*m[2][0] + m[0][3]*m[1][0]*m[2][2] - m[0][0]*m[1][3]*m[2][2] - m[0][2]*m[1][0]*m[2][3] + m[0][0]*m[1][2]*m[2][3];
  temp.m[2][0] = m[1][1]*m[2][3]*m[3][0] - m[1][3]*m[2][1]*m[3][0] + m[1][3]*m[2][0]*m[3][1] - m[1][0]*m[2][3]*m[3][1] - m[1][1]*m[2][0]*m[3][3] + m[1][0]*m[2][1]*m[3][3];
  temp.m[2][1] = m[0][3]*m[2][1]*m[3][0] - m[0][1]*m[2][3]*m[3][0] - m[0][3]*m[2][0]*m[3][1] + m[0][0]*m[2][3]*m[3][1] + m[0][1]*m[2][0]*m[3][3] - m[0][0]*m[2][1]*m[3][3];
  temp.m[2][2] = m[0][1]*m[1][3]*m[3][0] - m[0][3]*m[1][1]*m[3][0] + m[0][3]*m[1][0]*m[3][1] - m[0][0]*m[1][3]*m[3][1] - m[0][1]*m[1][0]*m[3][3] + m[0][0]*m[1][1]*m[3][3];
  temp.m[2][3] = m[0][3]*m[1][1]*m[2][0] - m[0][1]*m[1][3]*m[2][0] - m[0][3]*m[1][0]*m[2][1] + m[0][0]*m[1][3]*m[2][1] + m[0][1]*m[1][0]*m[2][3] - m[0][0]*m[1][1]*m[2][3];
  temp.m[3][0] = m[1][2]*m[2][1]*m[3][0] - m[1][1]*m[2][2]*m[3][0] - m[1][2]*m[2][0]*m[3][1] + m[1][0]*m[2][2]*m[3][1] + m[1][1]*m[2][0]*m[3][2] - m[1][0]*m[2][1]*m[3][2];
  temp.m[3][1] = m[0][1]*m[2][2]*m[3][0] - m[0][2]*m[2][1]*m[3][0] + m[0][2]*m[2][0]*m[3][1] - m[0][0]*m[2][2]*m[3][1] - m[0][1]*m[2][0]*m[3][2] + m[0][0]*m[2][1]*m[3][2];
  temp.m[3][2] = m[0][2]*m[1][1]*m[3][0] - m[0][1]*m[1][2]*m[3][0] - m[0][2]*m[1][0]*m[3][1] + m[0][0]*m[1][2]*m[3][1] + m[0][1]*m[1][0]*m[3][2] - m[0][0]*m[1][1]*m[3][2];
  temp.m[3][3] = m[0][1]*m[1][2]*m[2][0] - m[0][2]*m[1][1]*m[2][0] + m[0][2]*m[1][0]*m[2][1] - m[0][0]*m[1][2]*m[2][1] - m[0][1]*m[1][0]*m[2][2] + m[0][0]*m[1][1]*m[2][2];
  float scale=1/temp.determinant();
  temp.scale(scale,scale,scale);
  return temp;
}

float d_matrix::determinant()
{
  return
    m[0][3] * m[1][2] * m[2][1] * m[3][0]-m[0][2] * m[1][3] * m[2][1] * m[3][0]-m[0][3] * m[1][1] * m[2][2] * m[3][0]+m[0][1] * m[1][3] * m[2][2] * m[3][0]+
    m[0][2] * m[1][1] * m[2][3] * m[3][0]-m[0][1] * m[1][2] * m[2][3] * m[3][0]-m[0][3] * m[1][2] * m[2][0] * m[3][1]+m[0][2] * m[1][3] * m[2][0] * m[3][1]+
    m[0][3] * m[1][0] * m[2][2] * m[3][1]-m[0][0] * m[1][3] * m[2][2] * m[3][1]-m[0][2] * m[1][0] * m[2][3] * m[3][1]+m[0][0] * m[1][2] * m[2][3] * m[3][1]+
    m[0][3] * m[1][1] * m[2][0] * m[3][2]-m[0][1] * m[1][3] * m[2][0] * m[3][2]-m[0][3] * m[1][0] * m[2][1] * m[3][2]+m[0][0] * m[1][3] * m[2][1] * m[3][2]+
    m[0][1] * m[1][0] * m[2][3] * m[3][2]-m[0][0] * m[1][1] * m[2][3] * m[3][2]-m[0][2] * m[1][1] * m[2][0] * m[3][3]+m[0][1] * m[1][2] * m[2][0] * m[3][3]+
    m[0][2] * m[1][0] * m[2][1] * m[3][3]-m[0][0] * m[1][2] * m[2][1] * m[3][3]-m[0][1] * m[1][0] * m[2][2] * m[3][3]+m[0][0] * m[1][1] * m[2][2] * m[3][3];
}

void d_matrix::remove_scale()
{
  d_vector xvec = get_hori_i().normalise();
  d_vector yvec = get_hori_j().normalise();
  d_vector zvec = get_hori_k().normalise();

  m[0][0]=xvec.x; m[1][0]=xvec.y; m[2][0]=xvec.z;
  m[0][1]=yvec.x; m[1][1]=yvec.y; m[2][1]=yvec.z;
  m[0][2]=zvec.x; m[1][2]=zvec.y; m[2][2]=zvec.z;
}

void d_matrix::extract_euler(float &x, float &y, float &z)
{
  d_matrix t=*this;
  t.remove_scale();
  if (t.m[2][2]==0) x=0;
  else x = atan(t.m[1][2]/t.m[2][2])*RAD_CONV;
  y = asin(-t.m[0][2])*RAD_CONV;
  if (t.m[0][0]==0) z=0;
  else z=atan(t.m[0][1]/t.m[0][0])*RAD_CONV;


  /*d_vector xvec = get_hori_i().normalise();
    d_vector yvec = get_hori_j().normalise();
    d_vector zvec = get_hori_k().normalise();
    float d1,d2;
    xvec.get_euler(x,d1,d2);
    cerr<<x<<" "<<d1<<" "<<d2<<endl;
    yvec.get_euler(d1,y,d2);
    cerr<<d1<<" "<<y<<" "<<d2<<endl;
    zvec.get_euler(d1,d2,z);
    cerr<<d1<<" "<<d2<<" "<<z<<endl;*/
}

void d_matrix::aim(d_vector v, d_vector up)
{
  v.normalise();
  d_vector l=v.cross(up);
  d_vector u=v.cross(l);

  m[0][0]=v.x; m[0][1]=v.y; m[0][2]=v.z;
  m[1][0]=l.x; m[1][1]=l.y; m[1][2]=l.z;
  m[2][0]=u.x; m[2][1]=u.y; m[2][2]=u.z;
}

ostream &geogame::operator<<(ostream &os, d_matrix const &om)
{
  for (int j=0; j<4; j++)
    for (int i=0; i<4; i++)
      os<<om.m[i][j]<<" ";

  return os;
}

/*
  void d_axis::aimx(d_vector a, d_vector up)
  {
  if (up.mag()!=1) up.normalise();
  if (a.mag()!=1) a.normalise();
  i=a;
  if (i==up) up.x+=0.000001;
  j=i.cross(up);
  k=i.cross(j);
  }
*/

void d_bounding_box::expand(d_vector v)
{
  if (m_empty)
    {
      min=v;
      max=v;
      m_empty=false;
    }

  if (v.x<min.x) min.x=v.x;
  if (v.y<min.y) min.y=v.y;
  if (v.z<min.z) min.z=v.z;

  if (v.x>=max.x) max.x=v.x;
  if (v.y>=max.y) max.y=v.y;
  if (v.z>=max.z) max.z=v.z;
}

void d_bounding_box::expand(d_bounding_box v)
{
  expand(v.min);
  expand(d_vector(v.max.x,v.min.y,v.min.z));
  expand(d_vector(v.min.x,v.max.y,v.min.z));
  expand(d_vector(v.max.x,v.max.y,v.min.z));
  expand(d_vector(v.min.x,v.min.y,v.max.z));
  expand(d_vector(v.max.x,v.min.y,v.max.z));
  expand(d_vector(v.min.x,v.max.y,v.max.z));
  expand(v.max);
}

void d_bounding_box::expandby(float a)
{
  max.x+=a; max.y+=a; max.z+=a;
  min.x-=a; min.y-=a; min.z-=a;
}

bool d_bounding_box::inside(d_vector p) const
{
  return (p.x>min.x && p.x<max.x &&
	  p.y>min.y && p.y<max.y &&
	  p.z>min.z && p.z<max.z);
}

bool d_bounding_box::inside(d_vector p, float radius) const
{
  return (p.x+radius>min.x && p.x-radius<max.x &&
	  p.y+radius>min.y && p.y-radius<max.y &&
	  p.z+radius>min.z && p.z-radius<max.z);
}

bool inside(d_bounding_box v)
{
  return true;
}
