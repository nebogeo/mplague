/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "dada.h"
#include "entity.h"
#include <string>
#include <vector>

#ifndef KEY
#define KEY

namespace geogame {

class pickup : public entity
{
public:
  pickup(const d_vector &position, float lag);
  virtual ~pickup();
  
  virtual void update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist);
  virtual void render();
  virtual bool check_collide(const d_vector &pos, float radius, bool damage=false);
  virtual float get_mass() { return 0; }
  
  void set_capture_sound(int s) { m_capure_sound=s; }
  
 protected:
  float m_max_speed;
  float m_max_speed_sq;
  bool  m_following_mode;
  float m_follow_dist_sq;
  int	m_capure_sound;
};

 class key : public pickup
 {
 public:
   key(const d_vector &position, float lag);
   virtual ~key();

   virtual void render();
   virtual void update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist);
 };

 class weapon_pickup : public pickup
 {
 public:
   weapon_pickup(const d_vector &position, float lag);
   virtual ~weapon_pickup();

   virtual void render();
   virtual void update(const list<entity*> &world, const d_vector &playerpos, list<entity_command> &cmdlist);
 };
}
#endif
